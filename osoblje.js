let osoblje=(function(){

    let ucitajOsoblje=(function(){
        $.ajax({
            url: "/osobe",
            type: "GET",
            success: function (response) {
                $("#tabelaOsoba > tbody > tr:not('tr:first-of-type()')").remove();
                response.forEach(osoba => {
                $("#tabelaOsoba").find('tbody')
                    .append($('<tr>')
                        .append($('<td>')
                        .text(osoba.ime)
                        )
                        .append($('<td>')
                        .text(osoba.lokacija)
                        )
                    );
                    });
                setTimeout(ucitajOsoblje,3000);
            }
        });
    });
    return{
        ucitajOsoblje: ucitajOsoblje
    }

})();
osoblje.ucitajOsoblje();