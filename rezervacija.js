var mjeseci = ["Januar", "Februar", "Mart", "April", "Maj", "Juni", "Juli", "Avgust", "Septembar", "Oktobar", "Novembar", "Decembar"];
var mjesec = document.getElementById("mjesec");
var tabela = document.getElementById("tabelica");
var trenutni = 0;
var kliknuto = false;
var zauzeca = [];
var checkbox=document.getElementById("checkB");
checkbox.checked=true;
let Kalendar = (function () {
    
        var sljedeci = (function () {
        var btn_s = document.getElementById("s");
        return function () {
            if (trenutni < 11) {
                trenutni++;
            }
            else btn_s.disabled = true;

            mjesec.innerHTML = mjeseci[trenutni] + ":";
            iscrtajKalendar(tabela, trenutni);
            clicki();
            var btn_p = document.getElementById("p");
            btn_p.disabled = false;
        }

    })();
    var prethodni = (function () {
        var btn_p = document.getElementById("p");
        return function () {
            if (trenutni > 0) {
                trenutni--;
            }
            else btn_p.disabled = true;
            mjesec.innerHTML = mjeseci[trenutni] + ":";
            iscrtajKalendar(tabela, trenutni);
            clicki();
            var btn_s = document.getElementById("s");
            btn_s.disabled = false;
        }

    })();

    function iscrtajKalendar(kalendarRef, mjesec) {
        var celije = document.getElementsByClassName("sala");
        for (let index = 0; index < celije.length; index++) {
            celije[index].innerHTML = "";
        }
        var dani_mj = new Date(2019, mjesec + 1, 0).getDate();
        var p_celija = new Date(2019, mjesec, 0).getDay();
        var brojac = 1;
        for (let index = p_celija; index < dani_mj + p_celija; index++) {
            celije[index].innerHTML = brojac;
            brojac++;

        }
    }
    function obojiZauzeca(kalendarRef, mjesec, naziv, pocetak, kraj,predavac) {
        var celije = document.getElementsByClassName("sala");
        for (let index = 0; index < celije.length; index++) {
            if (celije[index].innerHTML != "") {
                celije[index].style.backgroundColor = "greenyellow";
            }
            else {
                celije[index].style.backgroundColor = "white";
            }
        }
        if (document.getElementById("checkB").checked) {
            for (let i = 0; i < zauzeca.length; i++) {
                if (predavac==zauzeca[i].predavac&&naziv == zauzeca[i].naziv&&zauzeca[i].datum==undefined) {
                    if (zauzeca[i].semestar == "ljetni") {
                        if (mjesec > 0 && mjesec < 5) {
                            var sat = zauzeca[i].pocetak;
                            var dijeloviP = sat.split(":");
                            var satiP = parseInt(dijeloviP[0]);
                            var sat1 = zauzeca[i].kraj;
                            var dijeloviK = sat1.split(":");
                            var satiK = parseInt(dijeloviK[0]);
                            if ((pocetak < kraj && kraj >= satiP && kraj <= satiK) || (pocetak >= satiP && pocetak <= satiK && kraj > pocetak) ||
                                (pocetak <= satiP && kraj >= satiK)) {
                                for (let x = 0; x < celije.length; x++) {
                                    if (celije[x].innerHTML != "" && x%7 ==zauzeca[i].dan)
                                        celije[x].style.backgroundColor = "orangered";
                                }
                            }
                        }

                    }
                    else {
                        if (mjesec > 8 || mjesec == 0) {
                            var sati = zauzeca[i].pocetak;
                            var dijeloviPoc = sati.split(":");
                            var satiPoc = parseInt(dijeloviPoc[0]);
                            var sati1 = zauzeca[i].kraj;
                            var dijeloviKraj = sati1.split(":");
                            var satiKraj = parseInt(dijeloviKraj[0]);
                            if ((pocetak < kraj && kraj >= satiPoc && kraj <= satiKraj) || (pocetak >= satiPoc && pocetak <= satiKraj && kraj > pocetak) ||
                                (pocetak <= satiPoc && kraj >= satiKraj)) {
                                for (let x = 0; x < celije.length; x++) {
                                    if (celije[x].innerHTML != ""&&x%7==zauzeca[i].dan)
                                        celije[x].style.backgroundColor = "orangered";
                                }
                            }
                        }
                    }
                }
            }
        }
        else {
            for (let i = 0; i < zauzeca.length; i++) {
                if (zauzeca[i].datum != undefined) {
                    var dijelovi = zauzeca[i].datum.split('.');
                    var dan = parseInt(dijelovi[0]);
                    var mj = parseInt(dijelovi[1])-1;
                    if (predavac==zauzeca[i].predavac&&zauzeca[i].naziv == naziv) {
                        if (trenutni == mj) {
                            var sati = zauzeca[i].pocetak;
                            var dijeloviPoc = sati.split(":");
                            var satiPoc = parseInt(dijeloviPoc[0]);
                            var sati1 = zauzeca[i].kraj;
                            var dijeloviKraj = sati1.split(":");
                            var satiKraj = parseInt(dijeloviKraj[0]);
                            if((pocetak < kraj && kraj >= satiPoc && kraj <= satiKraj) || (pocetak >= satiPoc && pocetak <= satiKraj && kraj > pocetak) ||
                            (pocetak <= satiPoc && kraj >= satiKraj)){
                                for (let x = 0; x < celije.length; x++)
                                    if (celije[x].innerHTML == dan)
                                        celije[x].style.backgroundColor = "orangered";
                            }
                        }
                    }
                }
            }
        }
    }
    function clicki() {
        var dlista = document.getElementById("dlista");
        var o_sala = dlista.options[dlista.selectedIndex].text;
        let profLista = document.getElementById("profLista");
        let prof = profLista.options[profLista.selectedIndex].value;
        var poc = document.getElementById("pocetak").value;
        var kr = document.getElementById("kraj").value;
        var vrijemePoc = poc.split(":");
        var pocetak = parseInt(vrijemePoc[0]);
        var vrijemeKraj = kr.split(":");
        var kraj = parseInt(vrijemeKraj[0]);
        obojiZauzeca(null, trenutni, o_sala, pocetak, kraj,prof);
    }
    function ucitajPodatke(periodicna, vandredna) {
        if(zauzeca.length=!0)
            zauzeca.length=0;
        periodicna.forEach(element => {
            zauzeca.push(element);
        });
        vandredna.forEach(element => {
            zauzeca.push(element);
        });
        return zauzeca;
    }
    return {
        sljedeci: sljedeci,
        prethodni: prethodni,
        iscrtajKalendar: iscrtajKalendar,
        obojiZauzeca: obojiZauzeca,
        click: clicki,
        ucitajPodatke: ucitajPodatke
    }
}());
Kalendar.iscrtajKalendar(tabela, trenutni);
var sljedeci = Kalendar.sljedeci;
var prethodni = Kalendar.prethodni;

