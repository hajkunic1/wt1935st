const express = require('express');
const app = express();
const fs = require('fs');
const db = require('./db.js');
const sequelize = require('sequelize');
app.use(express.static(__dirname));
app.get("/", function (req, res) {
    res.sendFile(__dirname + "/pocetna.html");
});
app.get("/osoblje", function (req, res) {
    db.sequelize.query("select * from osoblje", { plain: false }).then(function (osobe) {
        res.send(osobe[0]);
    })
});
app.get("/osobe", function (req, res) {
    let osoblje = [];
    let termini = [];
    db.sequelize.query("SELECT * FROM osoblje").then(function (osobe) {
        osobe[0].forEach(el => {
            osoblje.push(el);
        });
        db.sequelize.query("SELECT o.ime,o.prezime,s.naziv,t.redovni,t.dan,t.datum,t.semestar,t.pocetak,t.kraj,s.naziv from osoblje o,termin t,rezervacija r,sala s WHERE o.id=r.osoba and s.id=r.sala and r.termin=t.id").then(function (termin) {
            termin[0].forEach(el => {
                termini.push(el);
            });
            let kraj = [];
            termini.forEach(el => {
                if (el.redovni == 1) {
                    let dan = new Date(Date.now());
                    let lokacija="";
                    dan.setFullYear(2019);
                    if (dan.getDay() == el.dan && dan.getHours() >= el.pocetak.split(":")[0] && dan.getHours() <= el.kraj.split(":")[0]) {
                        lokacija=el.naziv;
                    }
                    else lokacija="Kancelarija";
                    let objekat={
                        ime: el.ime+" "+el.prezime,
                        lokacija: lokacija
                    }
                    kraj.push(objekat);
                }
                else {
                    let dan = new Date();
                    let lokacija ="";
                    dan.setFullYear(2019);
                    let datum=el.datum.split(".");
                    if (datum[0] == dan.getDate()&&datum[1]==(dan.getMonth()+1)&&datum[2]==dan.getFullYear() && dan.getHours() >= el.pocetak.split(":")[0] && dan.getHours() <= el.kraj.split(":")[0]) {
                        lokacija=el.naziv;
                    }
                    else lokacija="Kancelarija";
                    let objekat = {
                        ime: el.ime + " " + el.prezime,
                        lokacija: lokacija
                    }
                    kraj.push(objekat);
                    
                }
            });
            res.send(kraj);
        });
    });
});
app.get("/zauzeca", function (req, res) {
    db.sequelize.query("SELECT o.ime,o.prezime,s.naziv,t.redovni,t.dan,t.datum,t.semestar,t.pocetak,t.kraj,s.naziv from osoblje o,termin t,rezervacija r,sala s WHERE o.id=r.osoba and s.id=r.sala and r.termin=t.id").then(function (zauzeca) {
        let objekat = { periodicna: [], vanredna: [] };
        zauzeca[0].forEach(el => {
            let pom = {
                pocetak: el.pocetak,
                kraj: el.kraj,
                naziv: el.naziv,
                predavac: el.ime + " " + el.prezime
            }
            if (el.redovni == 1) {
                pom["dan"] = el.dan;
                pom["semestar"] = el.semestar;
                objekat.periodicna.push(pom);
            }
            else {
                pom["datum"] = el.datum;
                objekat.vanredna.push(pom);
            }

        });
        res.send(objekat);
    });

});
app.get("/sale", function (req, res) {
    db.sala.findAll().then(function (sale) {
        res.send(sale);
    });

})
app.post("/rezervacija.html", function (req, res) {
    let reqBody = '';
    req.on('data', function (data) {
        reqBody += data;
    });
    req.on('end', function () {
        let parametri = JSON.parse(reqBody);
        db.sequelize.query("SELECT o.ime,o.prezime,s.naziv,t.redovni,t.dan,t.datum,t.semestar,t.pocetak,t.kraj,s.naziv from osoblje o,termin t,rezervacija r,sala s WHERE o.id=r.osoba and s.id=r.sala and r.termin=t.id").then(function (zauzeca) {
            let objekat = { periodicna: [], vanredna: [] };
            zauzeca[0].forEach(el => {
                let pom = {
                    pocetak: el.pocetak,
                    kraj: el.kraj,
                    naziv: el.naziv,
                    predavac: el.predavac
                }
                if (el.redovni == 1) {
                    pom["dan"] = el.dan;
                    pom["semestar"] = el.semestar;
                    objekat.periodicna.push(pom);
                }
                else {
                    pom["datum"] = el.datum;
                    objekat.vanredna.push(pom);
                }

            });
            var niz = objekat["vanredna"];
            var niz2 = objekat["periodicna"];
            var da_li_je_periodicna = parametri["per"];
            delete parametri["per"];

            let ima_li_u_van = false, ima_li_u_per = false;
            let pom = parametri.datum.split(".");
            let dan = pom[0];
            let mjesec = pom[1];
            let datum_pom = new Date(2019, parseInt(mjesec) - 1, parseInt(dan) + 1);
            dan = datum_pom.getDay() - 2;
            for (let index = 0; index < niz2.length; index++) {
                if (niz2[index].naziv == parametri.naziv) {
                    if (niz2[index].dan == dan) {
                        if ((niz2[index].semestar == "zimski" && (mjesec == 1 || mjesec > 9)) ||
                            (niz2[index].semestar == "ljetni" && (mjesec > 1 && mjesec < 7))) {
                            if ((parametri.pocetak == niz2[index].pocetak && parametri.kraj == niz2[index].kraj) ||
                                (parametri.pocetak < niz2[index].pocetak && parametri.kraj > niz2[index].kraj) ||
                                (parametri.pocetak > niz2[index].pocetak && parametri.pocetak < niz2[index].kraj &&
                                    parametri.kraj < niz2[index].kraj && parametri.kraj > niz2[index].pocetak) ||
                                (parametri.pocetak >= niz2[index].pocetak && parametri.pocetak <= niz2[index].kraj && parametri.kraj > niz2[index].kraj) ||
                                (parametri.kraj >= niz2[index].pocetak && parametri.kraj <= niz2[index].kraj && parametri.pocetak < niz2[index].pocetak)
                            ) {
                                ima_li_u_per = true;
                                res.status(500).send(niz2[index].predavac);
                            }
                        }
                    }
                }
            }
            for (let index = 0; index < niz.length; index++) {
                if (niz[index].datum == parametri.datum && niz[index].naziv == parametri.naziv) {
                    if ((parametri.pocetak == niz[index].pocetak && parametri.kraj == niz[index].kraj) ||
                        (parametri.pocetak < niz[index].pocetak && parametri.kraj > niz[index].kraj) ||
                        (parametri.pocetak > niz[index].pocetak && parametri.pocetak < niz[index].kraj &&
                            parametri.kraj < niz[index].kraj && parametri.kraj > niz[index].pocetak) ||
                        (parametri.pocetak >= niz[index].pocetak && parametri.pocetak <= niz[index].kraj && parametri.kraj > niz[index].kraj) ||
                        (parametri.kraj >= niz[index].pocetak && parametri.kraj <= niz[index].kraj && parametri.pocetak < niz[index].pocetak)
                    ) {
                        ima_li_u_van = true;
                        res.status(500).send(niz[index].predavac);
                    }
                }
            }
            if (!ima_li_u_per && da_li_je_periodicna) {
                let semestar;
                if (da_li_je_periodicna) {
                    if (mjesec == 1 || mjesec > 9) {
                        semestar = "zimski";
                    }
                    else semestar = "ljetni";
                }
                let kraj = {
                    dan: dan,
                    semestar: semestar,
                    pocetak: parametri.pocetak,
                    kraj: parametri.kraj,
                    naziv: parametri.naziv,
                    predavac: parametri.predavac
                }
                db.termin.create({ redovni: 1, semestar: kraj.semestar, pocetak: kraj.pocetak, kraj: kraj.kraj, dan: kraj.dan }).then(function (termin) {
                    let ime = kraj.predavac.split(" ")[0];
                    let prezime = kraj.predavac.split(" ")[1];
                    db.osoblje.findOne({ where: { ime: ime, prezime: prezime } }).then(function (osoba) {
                        db.sala.findOne({ where: { naziv: kraj.naziv } }).then(function (sala) {
                            db.rezervacije.create({}).then(function (rezervacija) {
                                rezervacija.setTermini(termin);
                                rezervacija.setSale(sala);
                                rezervacija.setRezervacija(osoba);
                                return new Promise(function (resolve, reject) {
                                    resolve(rezervacija);
                                    res.status(200).send();
                                });
                            });
                        });
                    });
                })
            }

            if (!ima_li_u_van && !ima_li_u_per && da_li_je_periodicna == false) {
                let kraj = {
                    datum: parametri.datum,
                    pocetak: parametri.pocetak,
                    kraj: parametri.kraj,
                    naziv: parametri.naziv,
                    predavac: parametri.predavac
                }
                db.termin.create({ redovni: 0, datum: kraj.datum, pocetak: kraj.pocetak, kraj: kraj.kraj }).then(function (termin) {
                    let ime = kraj.predavac.split(" ")[0];
                    let prezime = kraj.predavac.split(" ")[1];
                    db.osoblje.findOne({ where: { ime: ime, prezime: prezime } }).then(function (osoba) {
                        db.sala.findOne({ where: { naziv: kraj.naziv } }).then(function (sala) {
                            db.rezervacije.create({}).then(function (rezervacija) {
                                rezervacija.setTermini(termin);
                                rezervacija.setSale(sala);
                                rezervacija.setRezervacija(osoba);
                                return new Promise(function (resolve, reject) {
                                    resolve(rezervacija);
                                    res.status(200).send();
                                });
                            });
                        });
                    });
                })
            }
        });


    });
});
app.get("/json", function (req, res) {
    res.sendFile(__dirname + "/zauzeca.json");
});
app.listen(8080);
console.log("Pokrenut server na portu 8080");