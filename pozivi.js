var xmlhttp = new XMLHttpRequest();

var brojac=0;
var bttn_s=document.getElementById("sljedeci");
var bttn_p=document.getElementById("prethodni");
var url=[
    "/slike/slika1.jpg",
    "/slike/slika2.jpg",
    "/slike/slika3.jpg",
    "/slike/slika4.jpg",
    "/slike/slika5.jpg",
    "/slike/slika6.jpg",
    "/slike/slika7.jpg",
    "/slike/slika8.jpg",
    "/slike/slika9.jpg",
    "/slike/slika10.jpg"
];

let pozivi = function () {
    function ucitajPodatke() {
        $.ajax({
            url: "/zauzeca",
            type: "GET",
            success: function (response) {
                Kalendar.ucitajPodatke(response["periodicna"],response["vanredna"]);
                $("#potvrdi").click();
            }
        });
    };
    function ucitajProfIzBaze(){
        $.ajax({
            url: "/osoblje",
            type: "GET",
            success: function (response) {
                let selectProf=document.getElementById("profLista");
                response.forEach(osoba => {
                    let option=document.createElement("option");
                    option.text=osoba.ime+" "+osoba.prezime;
                    selectProf.add(option);
                });
            }
        });
    }
    function ucitajSaleIzBaze(){
        $.ajax({
            url: "/sale",
            type: "GET",
            success: function (response) {
                let selectSala=document.getElementById("dlista");
                response.forEach(sala => {
                    let option=document.createElement("option");
                    option.text=sala.naziv;
                    selectSala.add(option);
                });
            }
        });
    }
   

    function rezrevisiSalu(tekst) {
        if(tekst<10) tekst='0'+tekst;
        let mjesec='';
        if(trenutni+1<10)mjesec='0'+(trenutni+1);
        let ddl = document.getElementById("dlista");
        let sel_item = ddl.options[ddl.selectedIndex].value;
        let starting_time = document.getElementById("pocetak").value;
        let ending_time = document.getElementById("kraj").value;
        let cb_checked=checkbox.checked;
        let profLista = document.getElementById("profLista");
        let predavac = profLista.options[profLista.selectedIndex].value;
        let data = {
            datum: tekst + "." + mjesec + "." + "2019",
            pocetak: starting_time,
            kraj: ending_time,
            naziv: sel_item,
            predavac: predavac,
            per:cb_checked
        };
        let cao = JSON.stringify(data);
        $.ajax({
            url: "/rezervacija.html",
            type: "POST",
            success: function (response) {
                pozivi.ucitajPodatke();
                alert("Uspješno rezervisana sala!");
            },
            data: cao,
            error: function () {
                alert("Rezervacija nije uspjela. Salu "+ sel_item + " je rezervisao "+predavac+" za navedeni datum "+tekst+"/"+(trenutni+1)+"/2019 i termin od " + starting_time + " do " + ending_time + ".");
            }
        });

    };
    function sljedeci(){
        if(brojac<4){
            bttn_p.disabled=false;
            brojac++;
            ucitajSlike();
        }
        else bttn_s.disabled=true;
    }
    function prethodni(){
        if(brojac>0){
            bttn_s.disabled=false;
            brojac--;
            if(brojac==0){
                $("#prva_slika").attr("src",url[0]); 
                $("#druga_slika").attr("src",url[1]); 
                $("#treca_slika").attr("src",url[2]);
            }
            else if(brojac==1){
                $("#prva_slika").attr("src",url[3]); 
                $("#druga_slika").attr("src",url[4]); 
                $("#treca_slika").attr("src",url[5]);
            }
            else if(brojac==2){
                $("#druga_slika").removeClass("sakrij");
                $("#treca_slika").removeClass("sakrij");
                $("#prva_slika").attr("src",url[6]); 
                $("#druga_slika").attr("src",url[7]); 
                $("#treca_slika").attr("src",url[8]);
            }
        }
        else bttn_p.disabled=true;
    }
    function ucitajSlike(){  
            if(brojac==0){
                $.ajax({ 
                    url : url[0], 
                    cache: true,
                    processData : false,
                }).always(function(){
                    $("#prva_slika").attr("src", url[0]);
                });   
                $.ajax({ 
                    url : url[1], 
                    cache: true,
                    processData : false,
                }).always(function(){
                    $("#druga_slika").attr("src", url[1]);
                });   
                $.ajax({ 
                    url : url[2], 
                    cache: true,
                    processData : false,
                }).always(function(){
                    $("#treca_slika").attr("src", url[2]);
                });   
                
            }
            else if(brojac==1){
                $.ajax({ 
                    url : url[3], 
                    cache: true,
                    processData : false,
                }).always(function(){
                    $("#prva_slika").attr("src", url[3]);
                });   
                $.ajax({ 
                    url : url[4], 
                    cache: true,
                    processData : false,
                }).always(function(){
                    $("#druga_slika").attr("src", url[4]);
                });   
                $.ajax({ 
                    url : url[5], 
                    cache: true,
                    processData : false,
                }).always(function(){
                    $("#treca_slika").attr("src", url[5]);
                });   
               
            }
            else if(brojac==2){
                $.ajax({ 
                    url : url[6], 
                    cache: true,
                    processData : false,
                }).always(function(){
                    $("#prva_slika").attr("src", url[6]);
                });   
                $.ajax({ 
                    url : url[7], 
                    cache: true,
                    processData : false,
                }).always(function(){
                    $("#druga_slika").attr("src", url[7]);
                });   
                $.ajax({ 
                    url : url[8], 
                    cache: true,
                    processData : false,
                }).always(function(){
                    $("#treca_slika").attr("src", url[8]);
                }); 
            }
             if(brojac==3){
                $("#druga_slika").addClass("sakrij");
                $("#treca_slika").addClass("sakrij");
                $.ajax({ 
                    url : url[9], 
                    cache: true,
                    processData : false,
                }).always(function(){
                    $("#prva_slika").attr("src", url[9]);
                });     
            }
    }
    return {
        ucitajPodatke: ucitajPodatke,
        rezrevisiSalu: rezrevisiSalu,
        sljedeci: sljedeci,
        prethodni: prethodni,
        ucitajSlike: ucitajSlike,
        ucitajProfIzBaze: ucitajProfIzBaze,
        ucitajSaleIzBaze: ucitajSaleIzBaze,
    }
    

}();