const Sequelize = require("sequelize");
//ako bude problema sa kreiranjem baze otkomentarisati kod ispod da bi se moglo raditi na lokalnoj bazi 
//const sequelize = new Sequelize("DBWT19","root","root",{host:"baza.db",dialect:"sqlite",logging:false});
// zatim zakomentarisati ovaj kod ispod i pokrenuti prvo node db.js pa onda node punjenje_tabela.js pa tek onda index
const sequelize = new Sequelize("DBWT19","root","root",{host:"127.0.0.1",dialect:"mysql",logging:false});
const db={};

db.Sequelize = Sequelize;  
db.sequelize = sequelize;

db.osoblje=sequelize.import(__dirname+'/modeli/osoblje.js');
db.rezervacije=sequelize.import(__dirname+'/modeli/rezervacija.js');
db.termin=sequelize.import(__dirname+'/modeli/termin.js');
db.sala=sequelize.import(__dirname+'/modeli/sala.js');

db.sala.belongsTo(db.osoblje,{as: 'osoblje',foreignKey:'zaduzenaOsoba'});
db.osoblje.hasOne(db.sala, {foreignKey:'zaduzenaOsoba'});

db.rezervacije.belongsTo(db.termin,{as: 'termini',foreignKey:'termin'});
db.termin.hasOne(db.rezervacije,{foreignKey:'termin'});

db.sala.hasMany(db.rezervacije,{foreignKey:'sala'});
db.rezervacije.belongsTo(db.sala,{as:'sale',foreignKey:'sala'});

db.osoblje.hasMany(db.rezervacije,{foreignKey:'osoba'});
db.rezervacije.belongsTo(db.osoblje,{as:'rezervacija',foreignKey:'osoba'});






module.exports=db;