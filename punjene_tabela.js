const db = require('./db.js')
db.sequelize.sync({force:true}).then(function(){
    inicializacija().then(function(){
        console.log("Gotovo kreiranje tabela i ubacivanje pocetnih podataka!");
        process.exit();
    });
});
function inicializacija(){
    var osobljeListaPromisea=[];
    var rezervacijeListaPromisea=[];
    var saleListaPromisea=[];
    var terminListaPromisea=[];
    return new Promise(function(resolve,reject){
    osobljeListaPromisea.push(db.osoblje.create({id: 1,ime:'Neko',prezime:'Nekic',uloga:'profesor'}));
    osobljeListaPromisea.push(db.osoblje.create({id: 2,ime:'Drugi',prezime:'Nekic',uloga:'asistent'}));
    osobljeListaPromisea.push(db.osoblje.create({id: 3,ime:'Test',prezime:'Test',uloga:'asistent'}));
    Promise.all(osobljeListaPromisea).then(function(osoblje){
        let prof=osoblje.filter(function(o){return o.id=='1'})[0];
        let asis=osoblje.filter(function(o){return o.id=='2'})[0];
        let asis1=osoblje.filter(function(o){return o.id=='3'})[0];
    
        saleListaPromisea.push(db.sala.create({id:1,naziv:'1-11'}).then(
            function(sale){
                sale.setOsoblje(prof);
                return new Promise(function(resolve,reject){resolve(sale)});
            }

        ));
        saleListaPromisea.push(db.sala.create({id:2,naziv:'1-15'}).then(
            function(sale){
                sale.setOsoblje(asis);
                return new Promise(function(resolve,reject){resolve(sale)});
            }

        ));
        Promise.all(saleListaPromisea).then(function(sale){
            let sala=sale.filter(function(s){return s.id=='1'})[0];
            terminListaPromisea.push(db.termin.create({id:1,redovni:false,datum:'01.01.2019',pocetak:"12:00",kraj:"13:00"}));
            terminListaPromisea.push(db.termin.create({id:2,redovni:true,dan: 0,semestar:'zimski',pocetak:"13:00",kraj:"14:00"}));
            Promise.all(terminListaPromisea).then(function(termini){
                let vanredni=termini.filter(function(t){return t.id=='1';})[0];
                let redovni=termini.filter(function(t){return t.id=='2';})[0];
                rezervacijeListaPromisea.push(db.rezervacije.create({id:'1'}).then(function(rezervacija){
                    rezervacija.setTermini(vanredni);
                    rezervacija.setSale(sala);
                    rezervacija.setRezervacija(prof);
                    return new Promise(function(resolve,reject){resolve(rezervacija)});
                }));
                rezervacijeListaPromisea.push(db.rezervacije.create({id:2}).then(function (rezervacija){
                    rezervacija.setTermini(redovni);
                    rezervacija.setSale(sala);
                    rezervacija.setRezervacija(asis1);
                    return new Promise(function(resolve,reject){resolve(rezervacija)});
                }));
            });

        });
    }).catch(function(err){console.log("Autori greska "+err);});   
    });
}
