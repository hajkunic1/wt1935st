
let assert = chai.assert;
describe('Kalendar', function() {
 describe('iscrtajKalentar()', function() {
   it('Februar treba imati 28 dana', function() {
     Kalendar.iscrtajKalendar(null,1);
     let celije=document.getElementsByClassName("sala");
     let brojac=0;
        for (let index = 0; index < celije.length; index++) {
            if(celije[index].innerHTML!="")
                brojac++;
        }
     assert.equal(brojac, 28,"Broj dana treba biti 28");
   });
   it('April treba imati 30 dana', function() {
    Kalendar.iscrtajKalendar(null,3);
    let celije=document.getElementsByClassName("sala");
    let brojac=0;
       for (let index = 0; index < celije.length; index++) {
           if(celije[index].innerHTML!="")
               brojac++;
       }
    assert.equal(brojac, 30,"Broj dana treba biti 30");
  });
  it('Trenutni mjesec treba da pocinje u petak', function() {
    Kalendar.iscrtajKalendar(null,10);
    let celije=document.getElementsByClassName("sala");
    assert.equal(celije[4].innerHTML, "1","Prvi dan bi trebao biti petak");
  });
  
  it('Trenutni mjesec treba da zavrsava u subotu ', function() {
    Kalendar.iscrtajKalendar(null,10);
    let celije=document.getElementsByClassName("sala");
    let brojac=0;
    for (let index = 0; index < celije.length; index++) {
        if(celije[index].innerHTML=="30") break;
            brojac++;
        
    }
    let subota=5;//subota je sa indexom 5 u svakom redu
    let zadnjiDan=brojac%7;
    assert.equal(zadnjiDan,subota,"Prvi dan bi trebao biti petak");
  });
  it('Januar treba da pocinje u utorak i treba imati 31 dan', function() {
    Kalendar.iscrtajKalendar(null,0);
    let celije=document.getElementsByClassName("sala");
    let brojac=0;
    for (let index = 0; index < celije.length; index++) {
        if(celije[index].innerHTML!="")
            brojac++;
    }
    assert.equal(celije[1].innerHTML,"1","Prvi dan bi trebao biti petak");
    assert.equal(brojac,31,"Broj dana treba biti 31");
  });
  it('Decembar treba imati 31 dan', function() {
    Kalendar.iscrtajKalendar(null,11);
    let celije=document.getElementsByClassName("sala");
    let brojac=0;
    for (let index = 0; index < celije.length; index++) {
        if(celije[index].innerHTML!="")
            brojac++;
    }
    assert.equal(brojac,31,"Prvi dan bi trebao biti petak");
  });
  it('Decembar treba pocinjati u nedjelju', function() {
    Kalendar.iscrtajKalendar(null,11);
    let celije=document.getElementsByClassName("sala");
    let brojac=0;
    for (let index = 0; index < celije.length; index++) {
        if(celije[index].innerHTML=="1")
            break;
        brojac++;
    }
    let nedjelja=6;
    assert.equal(brojac,nedjelja,"Prvi dan bi trebao biti petak");
  });
 });
 describe('obojiZauzeca()', function() {
    it('Ucitaj podatke nije pozvana, nije obojen niti jedan dan', function() {
      Kalendar.obojiZauzeca(null,0,"0-01","10:00","12:00");
      let celije=document.getElementsByClassName("sala");
      let brojac=0;
         for (let index = 0; index < celije.length; index++) {
             if(celije[index].style.backgroundColor!="orangered")
                 brojac++;
         }
      assert.equal(brojac, celije.length,"Broj dana treba biti 28");
    });
    it('Duple vrijednosti', function() {
        Kalendar.ucitajPodatke(
            [
                {
                    dan: 0,
                    semestar: "zimski",
                    pocetak: "10:00",
                    kraj: "12:00",
                    naziv: "0-01",
                    predavac: "Vedran Ljubovic"
                },
                {
                    dan: 0,
                    semestar: "zimski",
                    pocetak: "10:00",
                    kraj: "12:00",
                    naziv: "0-01",
                    predavac: "Vedran Ljubovic"
                }
            ],
            [{
            }]
        );
        Kalendar.iscrtajKalendar(null,0);
        Kalendar.obojiZauzeca(null,0,"0-01",10,12);
        let celije=document.getElementsByClassName("sala");
        let brojac=0;
           for (let index = 0; index < celije.length; index++) {
               if(celije[index].style.backgroundColor=="orangered"){
                   brojac++;
               }
           }
        assert.equal(brojac, 4,"Dan se treba obojiti bez obzira na duplikate");
      });
      it('Drugi semestar', function() {
        Kalendar.ucitajPodatke(
            [
                {
                    dan: 0,
                    semestar: "zimski",
                    pocetak: "10:00",
                    kraj: "12:00",
                    naziv: "0-01",
                    predavac: "Vedran Ljubovic"
                }
            ],
            [{
            }]
        );
        Kalendar.obojiZauzeca(null,2,"0-01",10,12);
        let celije=document.getElementsByClassName("sala");
        let brojac=0;
           for (let index = 0; index < celije.length; index++) {
               if(celije[index].style.backgroundColor=="orangered")
                   brojac++;
           }
        assert.equal(brojac,0,"Ne treba biti obojeno nista");
      });
      it('Sve zauzeto', function() {
        Kalendar.ucitajPodatke(
            [
                {
                    dan: 0,
                    semestar: "zimski",
                    pocetak: "10:00",
                    kraj: "12:00",
                    naziv: "0-01",
                    predavac: "Vedran Ljubovic"
                },
                {
                    dan: 1,
                    semestar: "zimski",
                    pocetak: "10:00",
                    kraj: "12:00",
                    naziv: "0-01",
                    predavac: "Vedran Ljubovic"
                },
                {
                    dan: 2,
                    semestar: "zimski",
                    pocetak: "10:00",
                    kraj: "12:00",
                    naziv: "0-01",
                    predavac: "Vedran Ljubovic"
                },
                {
                    dan: 3,
                    semestar: "zimski",
                    pocetak: "10:00",
                    kraj: "12:00",
                    naziv: "0-01",
                    predavac: "Vedran Ljubovic"
                },
                {
                    dan: 4,
                    semestar: "zimski",
                    pocetak: "10:00",
                    kraj: "12:00",
                    naziv: "0-01",
                    predavac: "Vedran Ljubovic"
                },
                {
                    dan: 5,
                    semestar: "zimski",
                    pocetak: "10:00",
                    kraj: "12:00",
                    naziv: "0-01",
                    predavac: "Vedran Ljubovic"
                },
                {
                    dan: 6,
                    semestar: "zimski",
                    pocetak: "10:00",
                    kraj: "12:00",
                    naziv: "0-01",
                    predavac: "Vedran Ljubovic"
                }
            ],
            [
            ]
        );
        Kalendar.obojiZauzeca(null,0,"0-01",10,12);
        let celije=document.getElementsByClassName("sala");
        let brojac=0;
           for (let index = 0; index < celije.length; index++) {
               if(celije[index].style.backgroundColor=="orangered")
                   brojac++;
           }
        assert.equal(brojac,31,"Sve treba biti obojeno");
      });
      it('Dvaput pozivati oboji zauzeca', function() {
        Kalendar.ucitajPodatke(
            [
                {
                    dan: 0,
                    semestar: "zimski",
                    pocetak: "10:00",
                    kraj: "12:00",
                    naziv: "0-01",
                    predavac: "Vedran Ljubovic"
                }
            ],
            [{
            }]
        );
        Kalendar.obojiZauzeca(null,0,"0-01",10,12);
        Kalendar.obojiZauzeca(null,0,"0-01",10,12);
        let celije=document.getElementsByClassName("sala");
        let brojac=0;
           for (let index = 0; index < celije.length; index++) {
               if(celije[index].style.backgroundColor=="orangered")
                   brojac++;
           }
        assert.equal(brojac,4,"Ne treba biti obojeno nista");
      });
      it('Dvaput pozvati funkcije ucitaj oboji', function() {
        Kalendar.ucitajPodatke(
            [
                {
                    dan: 0,
                    semestar: "zimski",
                    pocetak: "10:00",
                    kraj: "12:00",
                    naziv: "0-01",
                    predavac: "Vedran Ljubovic"
                }
            ],
            [{
            }]
        );
        Kalendar.obojiZauzeca(null,0,"0-01",10,12);
        Kalendar.ucitajPodatke(
            [
                {
                    dan: 4,
                    semestar: "zimski",
                    pocetak: "10:00",
                    kraj: "12:00",
                    naziv: "0-01",
                    predavac: "Vedran Ljubovic"
                }
            ],
            [{
            }]
        );
        Kalendar.obojiZauzeca(null,0,"0-01",10,12);
        let celije=document.getElementsByClassName("sala");
        let brojac=0;
           for (let index = 0; index < celije.length; index++) {
               if(celije[index].style.backgroundColor=="orangered")
                   brojac++;
           }
        assert.equal(brojac,4,"Prethodno oboji ne bi trebalo biti obojeno");
      });
      it('Dugi mjesec', function() {
        Kalendar.ucitajPodatke(
            [
                {
                    dan: 0,
                    semestar: "zimski",
                    pocetak: "10:00",
                    kraj: "12:00",
                    naziv: "0-01",
                    predavac: "Vedran Ljubovic"
                }
            ],
            [{
            }]
        );
        Kalendar.obojiZauzeca(null,2,"0-01",10,12);
        let celije=document.getElementsByClassName("sala");
        let brojac=0;
           for (let index = 0; index < celije.length; index++) {
               if(celije[index].style.backgroundColor=="orangered")
                   brojac++;
           }
        assert.equal(brojac,0,"Ne treba biti obojeno nista");
      });
      it('Vanredna zauzeca', function() {
        Kalendar.ucitajPodatke(
            [
                
            ],
            [
                {
                
                    datum: "21.01.2019",
                    pocetak: "12:00",
                    kraj: "13:00",
                    naziv: "1-02",
                    predavac: "Vedran Ljubovic"
                }
            ]
        );
        let cb=document.getElementById("checkB").checked=false;
        Kalendar.obojiZauzeca(null,0,"1-02",12,13);
        let celije=document.getElementsByClassName("sala");
        let brojac=0;
           for (let index = 0; index < celije.length; index++) {
               if(celije[index].style.backgroundColor=="orangered")
               {
                   brojac++;
               }
           }
        assert.equal(brojac,1,"Samo jedna sala treba biti obojena");
      });
      it('Vanredna zauzeca,sala nije zauzeta', function() {
        Kalendar.ucitajPodatke(
            [
                
            ],
            [
                {
                
                    datum: "21.01.2019",
                    pocetak: "12:00",
                    kraj: "13:00",
                    naziv: "1-02",
                    predavac: "Vedran Ljubovic"
                }
            ]
        );
        let cb=document.getElementById("checkB").checked=false;
        Kalendar.obojiZauzeca(null,0,"1-02",14,15);
        let celije=document.getElementsByClassName("sala");
        let brojac=0;
           for (let index = 0; index < celije.length; index++) {
               if(celije[index].style.backgroundColor=="orangered")
               {
                   brojac++;
               }
           }
        assert.equal(brojac,0,"Sala je slobodna");
      });
  });
});
